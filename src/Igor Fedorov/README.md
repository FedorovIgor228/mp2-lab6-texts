﻿# Лабораторная работа №6: Тексты

## Цели и задачи

В рамках лабораторной работы ставится задача разработки учебного редактора текстов с поддержкой следующих операций:
* выбор текста для редактирования (или создание нового текста);
* демонстрация текста на экране дисплея;
* поддержка средств указания элементов (уровней) текста;
* вставка, удаление и замена строк текста;
* запись подготовленного текста в файл.

### Условия и ограничения
При выполнении лабораторной работы использовались следующие основные предположения:
1. При планировании структуры текста в качестве самого нижнего уровня можно рассматривать уровень строк.
2. В качестве тестовых текстов можно рассматривать текстовые файлы программы.

### План работы
1. Разработка структуры хранения текстов.
2. Разработка методов для доступа к строкам текста.
3. Возможность модификации структуры текста.
4. Создание механизма для обхода текста.
5. Реализация итератора.
6. Копирование текста.
7. Реализация сборки мусора.
8. Тестирование.

Общая структура классов:

![](classes.png)


## Выполнение работы

### 1. Абстрактный класс `TDatValue`.

`tdatvalue.h`:
```cpp
#pragma once

class TDatValue;
typedef TDatValue *PTDatValue;

class TDatValue {
public:
	virtual TDatValue * GetCopy() = 0; // создание копии
	~TDatValue() {}
};
```

Класс звена текста `TTextLink`.

`ttextlink.h`:
```cpp
#pragma once
#include "TDatValue.h"
#include <iostream>
#include <string>

#define TextLineLength 30
#define MemSize 20

using namespace std;

class TText;
class TTextLink;
typedef TTextLink *PTTextLink;
typedef char TStr[TextLineLength];

class TTextMem {
	PTTextLink pFirst;     // указатель на первое звено
	PTTextLink pLast;      // указатель на последнее звено
	PTTextLink pFree;      // указатель на первое свободное звено
	friend class TTextLink;
};
typedef TTextMem *PTTextMem;

class TTextLink : public TDatValue {
protected:
	TStr Str;  // поле для хранения строки текста
	PTTextLink pNext, pDown;  // указатели по тек. уровень и на подуровень
	static TTextMem MemHeader; // система управления памятью
public:
	static void InitMemSystem(int size = MemSize); // инициализация памяти
	static void PrintFreeLink(void);  // печать свободных звеньев
	void * operator new (size_t size); // выделение звена
	void operator delete (void *pM);   // освобождение звена
	static void MemCleaner(TText &txt); // сборка мусора
	//конструкторы
	TTextLink(TStr s = NULL, PTTextLink pn = NULL, PTTextLink pd = NULL) {
		pNext = pn; pDown = pd;
		if (s != NULL) strcpy_s(Str, s); else Str[0] = '\0';
	}  

	TTextLink(string s) {
        pNext = nullptr; pDown = nullptr;
        strcpy_s(Str, s.c_str());
    }
	~TTextLink() {}
	bool IsAtom() { return pDown == NULL; } // проверка атомарности звена
	//Getter ы
	PTTextLink GetNext() { return pNext; }
	PTTextLink GetDown() { return pDown; }
	PTDatValue GetCopy() { return new TTextLink(Str, pNext, pDown); }
protected:
	virtual void Print(ostream &os) { os << Str; }
	friend class TText;
};
```

`TTextLink.cpp`:
```cpp
#include "..\include\TTextLink.h"
#include "..\include\TText.h"

TTextMem TTextLink::MemHeader;

void TTextLink::InitMemSystem(int size) {
	MemHeader.pFirst = (PTTextLink) new char[sizeof(TTextLink)*size];
	MemHeader.pFree = MemHeader.pFirst;
	MemHeader.pLast = MemHeader.pFirst + (size - 1);
	PTTextLink pLink = MemHeader.pFirst;
	for (int i = 0; i < size - 1; i++, pLink++) {
		pLink->pNext = pLink + 1;
	}
	pLink->pNext = nullptr;
}

void TTextLink::PrintFreeLink() {
	PTTextLink pLink = MemHeader.pFree;
	for (; pLink != nullptr; pLink = pLink->pNext)
		cout << pLink->Str << endl;
}

void* TTextLink::operator new (size_t size) {
	PTTextLink pLink = MemHeader.pFree;
	if (MemHeader.pFree != nullptr)
		MemHeader.pFree = pLink->pNext;
	return pLink;
}

void TTextLink:: operator delete (void *pM) {
	PTTextLink pLink = (PTTextLink)pM;
	pLink->pNext = MemHeader.pFree;
	MemHeader.pFree = pLink;
}

void TTextLink::MemCleaner(TText &txt) {
	//insert ^-^ 
	for (txt.Reset(); !txt.IsTextEnded(); txt.GoNext()) {
		txt.SetLine("^-^" + txt.GetLine());
	}
	//mark all free link
	PTTextLink pLink = MemHeader.pFree;
	for (; pLink != NULL; pLink = pLink->pNext) 
        strcpy_s(pLink->Str, "^-^");
	//
	pLink = MemHeader.pFirst;
	for (; pLink <= MemHeader.pLast; pLink++) {
		if (strstr(pLink->Str, "^-^") != nullptr) {
			strcpy_s(pLink->Str, pLink->Str + 3);
		}
		else delete pLink;
	}
}
```

### 2. Абстрактный класс `TDataCom`.

`tdatacom.h`:
```cpp
#pragma once

#define TextOK	0 // ошибок нет
// коды ситуаций
#define TextNoDown 101 // нет подуровня для текущей позиции
#define TextNoNext 102 // нет следующего раздела текущего уровня
#define TextNoPrev 103 // текущая позиция в начале текста
// коды ошибок
#define TextError -102 // ошибка в тексте
#define TextNoMem -101 // нет памяти

class TDataCom
{
protected:
	int RetCode; // Код завершения

	int SetRetCode(int ret) { 
		return RetCode = ret; 
	}
public:
	TDataCom() : RetCode(TextOK) {}
	virtual ~TDataCom() = 0 {}
	int GetRetCode()
	{
		int temp = RetCode;
		RetCode = TextOK;
		return temp;
	}
};
```

Класс для текста `TText`.

`ttext.h`:
```cpp
#pragma once

#include <stack>
#include <string>
#include <fstream>
#include "TTextLink.h"
#include "TDataCom.h"

class TText;
typedef TText *PTText;

class TText : public TDataCom {
protected:
	PTTextLink pFirst;      // указатель корня дерева
	PTTextLink pCurrent;      // указатель текущей строки
	stack< PTTextLink > Path; // стек траектории движения по тексту
	stack< PTTextLink > St;   // стек для итератора
	PTTextLink GetFirstAtom(PTTextLink pl); // поиск первого атома
	void PrintText(PTTextLink ptl);         // печать текста со звена ptl
	void PrintTextFile(PTTextLink ptl, ofstream& TxtFile); // печать в файл
	PTTextLink ReadText(ifstream &TxtFile); //чтение текста из файла
public:
	TText(PTTextLink pl = NULL);
	~TText() { pFirst = NULL; }
	PTText getCopy();
	// навигация
	int GoFirstLink(); // переход к первой строке
	int GoDownLink();  // переход к следующей строке по Down
	int GoNextLink();  // переход к следующей строке по Next
	int GoPrevLink();  // переход к предыдущей позиции в тексте
	// доступ
	string GetLine();   // чтение текущей строки
	void SetLine(string s); // замена текущей строки 
	// модификация
	void InsDownLine(string s);    // вставка строки в подуровень
	void InsDownSection(string s); // вставка раздела в подуровень
	void InsNextLine(string s);    // вставка строки в том же уровне
	void InsNextSection(string s); // вставка раздела в том же уровне
	void DelDownLine();        // удаление строки в подуровне
	void DelDownSection();     // удаление раздела в подуровне
	void DelNextLine();        // удаление строки в том же уровне
	void DelNextSection();     // удаление раздела в том же уровне
	// итератор
	int Reset();              // установить на первую звапись
	bool IsTextEnded() const;  // текст завершен?
	int GoNext();             // переход к следующей записи
	//работа с файлами
	void Read(char * pFileName);  // ввод текста из файла
	void Write(char * pFileName); // вывод текста в файл
	//печать
	void Print();             // печать текста
};
```

`TText.cpp`:
```cpp
#include "..\include\TText.h"

const int buf = 80;
static int TextLevel;

//конструктор
TText::TText(PTTextLink pl) {
	if (pl == nullptr) 
		pl = new TTextLink();
	pFirst = pl;
}

PTText TText::getCopy()
{
	PTTextLink pl1, pl2 = nullptr, pl = pFirst, cpl = nullptr;

	if (pFirst != nullptr) {
		while (!St.empty()) St.pop();
		while (true)
		{
			if (pl != nullptr) {
				pl = GetFirstAtom(pl);
				St.push(pl);
				pl = pl->GetDown();
			}
			else if (St.empty()) break;
			else {
				pl1 = St.top(); St.pop();
				if (strstr(pl1->Str, "Copy") == nullptr) {
					pl2 = new TTextLink("Copy", pl1, cpl);
					St.push(pl2);
					pl = pl1->GetNext();
					cpl = nullptr;
				}
				else {
					strcpy_s(pl1->Str, pl1->pNext->Str);
					pl1->pNext = cpl;
					cpl = pl1;
				}
			}
		}
		SetRetCode(TextOK);
	}
	else {
		SetRetCode(TextError);
	}
	return new TText(cpl);
}

//навигация
int TText::GoFirstLink(void) {
	while (!Path.empty()) 
		Path.pop();
	pCurrent = pFirst;
	if (pCurrent == nullptr)
		SetRetCode(TextError);
	else
		SetRetCode(TextOK);
	return RetCode;
}

int TText::GoDownLink(void) {
	SetRetCode(TextNoDown);
	if (pCurrent != nullptr)
		if (pCurrent->pDown != nullptr) {
			Path.push(pCurrent);
			pCurrent = pCurrent->pDown;
			SetRetCode(TextOK);
		}
	return RetCode;
}

int TText::GoNextLink(void) {
	SetRetCode(TextNoNext);
	if (pCurrent != nullptr)
		if (pCurrent->pNext != nullptr) {
			Path.push(pCurrent);
			pCurrent = pCurrent->pNext;
			SetRetCode(TextOK);
		}
	return RetCode;
}

int TText::GoPrevLink(void) {
	if (Path.empty())
		SetRetCode(TextNoPrev);
	else {
		pCurrent = Path.top();
		Path.pop();
		SetRetCode(TextOK);
	}
	return RetCode;
}

//доступ
string TText::GetLine(void) {
	if (pCurrent == nullptr)
		return string("");
	else
		return string(pCurrent->Str);
}

void TText::SetLine(string s) {
	if (pCurrent == nullptr)
		SetRetCode(TextError);
	else
		strcpy_s(pCurrent->Str, s.c_str());
}

//модификация
void TText::InsDownLine(string s) {
	if (pCurrent == nullptr)
		SetRetCode(TextError);
	else {
        TStr buf;
        strcpy_s(buf, s.c_str());
        pCurrent->pDown = new TTextLink(buf, pCurrent->pDown, nullptr);
        SetRetCode(TextOK);
	}
}

void TText::InsDownSection(string s) {
	if (pCurrent == nullptr)
		SetRetCode(TextError);
	else {
        TStr buf;
        strcpy_s(buf, s.c_str());
        pCurrent->pDown = new TTextLink(buf, nullptr, pCurrent->pDown);
		SetRetCode(TextOK);
	}
}

void TText::InsNextLine(string s) {
	if (pCurrent == nullptr)
		SetRetCode(TextError);
	else {
        TStr buf;
        strcpy_s(buf, s.c_str());
        pCurrent->pNext = new TTextLink(buf, pCurrent->pNext, nullptr);
		SetRetCode(TextOK);
	}
}
void TText::InsNextSection(string s) {
	if (pCurrent == nullptr)
		SetRetCode(TextError);
	else {
        TStr buf;
        strcpy_s(buf, s.c_str());
        pCurrent->pNext = new TTextLink(buf, nullptr, pCurrent->pNext);
		SetRetCode(TextOK);
	}
}

void TText::DelDownLine(void) {
    if (pCurrent == nullptr)
        SetRetCode(TextError);
    else if (pCurrent->pDown == nullptr)
        SetRetCode(TextNoDown);
    else if (pCurrent->pDown->IsAtom())
        pCurrent->pDown = pCurrent->pDown->pNext;
}

void TText::DelDownSection(void) {
    if (pCurrent == nullptr)
        SetRetCode(TextError);
    else if (pCurrent->pDown == nullptr)
        SetRetCode(TextNoDown);
    else
        pCurrent->pDown = nullptr;
}

void TText::DelNextLine(void) {
    if (pCurrent == nullptr)
        SetRetCode(TextError);
    else if (pCurrent->pNext == nullptr)
        SetRetCode(TextNoNext);
    else if (pCurrent->pNext->IsAtom())
        pCurrent->pNext = pCurrent->pNext->pNext;
}

void TText::DelNextSection(void) {
    if (pCurrent == nullptr)
        SetRetCode(TextError);
    else if (pCurrent->pNext == nullptr)
        SetRetCode(TextNoNext);
    else
        pCurrent->pNext = pCurrent->pNext->pNext;
}

//итератор
int TText::Reset(void) {
	while (!St.empty())
		St.pop();
	pCurrent = pFirst;
	if (pCurrent != nullptr) {
		St.push(pCurrent);
		if (pCurrent->pNext != nullptr)
			St.push(pCurrent->pNext);
		if (pCurrent->pDown != nullptr)
			St.push(pCurrent->pDown);
	}
	return IsTextEnded();
}

bool TText::IsTextEnded(void) const {
	return !St.size();
}

int TText::GoNext(void) {
	if (!IsTextEnded()) {
		pCurrent = St.top(); St.pop();
		if (pCurrent != pFirst) {
			if (pCurrent->pNext != nullptr)
				St.push(pCurrent->pNext);
			if (pCurrent->pDown != nullptr)
				St.push(pCurrent->pDown);
		}
	}
	return IsTextEnded();
}

//копирование текста
PTTextLink TText::GetFirstAtom(PTTextLink pl) {
	PTTextLink tmp = pl;
	while (!tmp->IsAtom()) {
		St.push(tmp);
		tmp = tmp->GetDown();
	}
	return tmp;
}

//печать
void TText::Print() {
	TextLevel = 0;
	PrintText(pFirst);
}

void TText::PrintText(PTTextLink ptl) {
	if (ptl != nullptr) {
		for (int i = 0; i < TextLevel; i++)
			cout << " ";
        cout << ptl->Str << endl;
		TextLevel++; PrintText(ptl->GetDown());
		TextLevel--; PrintText(ptl->GetNext());
	}
}

void TText::PrintTextFile(PTTextLink ptl, ofstream & TxtFile)
{
    if (ptl != nullptr) {
        for (int i = 0; i < TextLevel; i++)
            TxtFile << " ";
        TxtFile << ptl->Str << endl;
        TextLevel++; PrintTextFile(ptl->GetDown(), TxtFile);
        TextLevel--; PrintTextFile(ptl->GetNext(), TxtFile);
    }
}

//чтение
void TText::Read(char* pFileName) {
	ifstream TxtFile(pFileName);
	TextLevel = 0;
	if (&TxtFile != nullptr)
		pFirst = ReadText(TxtFile);
}

PTTextLink TText::ReadText(ifstream& TxtFile)
{
    string buf;
    PTTextLink ptl = new TTextLink();
    PTTextLink tmp = ptl;
    while (!TxtFile.eof())
    {
        getline(TxtFile, buf);
        if (buf.front() == '}')
            break;
        else if (buf.front() == '{')
            ptl->pDown = ReadText(TxtFile);
        else
        {
            ptl->pNext = new TTextLink(buf.c_str());
            ptl = ptl->pNext;
        }
    }
    ptl = tmp;
    if (tmp->pDown == nullptr)
    {
        tmp = tmp->pNext;
        delete ptl;
    }
    return tmp;
}

void TText::Write(char * pFileName)
{
    TextLevel = 0;
    ofstream TextFile(pFileName);
    PrintTextFile(pFirst, TextFile);
}
```
	
### 3. Демонстрационная программа

`main.cpp`

```cpp
#include "..\include\TText.h"

int main()
{
    TTextLink::InitMemSystem(14);
    TText text;
	
    text.Read("input.txt");
    text.GoFirstLink();
    text.GoDownLink();
    text.DelDownSection();

    TTextLink::MemCleaner(text);

    text.Print();
	getchar();
    text.Write("output.txt");
}
```
![](demo.jpg)

### 4. Проверка работоспособности при помощи Google Test Framework

Данные классы были протестированы с помощью фреймворка **Google Test**.

`test_TText.cpp`

```cpp
#include "../Text/include/TText.h"
#include <gtest.h>

TEST(TText, can_create_a_text_parameter)
{
	const string str = "Hello world!";
	TTextLink Link = TTextLink(str);
	PTTextLink pLink = &Link;
	TTextLink::InitMemSystem(1);
	TText T(pLink);
	T.GoFirstLink();
	EXPECT_EQ(T.GetLine(), str);
}

TEST(TText, can_add_down_a_line)
{
	TTextLink::InitMemSystem(2);
	TText T;
	const string str = "Hello world!";
	T.GoFirstLink();
	T.InsDownLine(str);
	T.GoDownLink();
	EXPECT_EQ(T.GetLine(), str);
}

TEST(TText, can_change_the_current_line)
{
	TTextLink::InitMemSystem(2);
	TText T;
	T.GoFirstLink();
	const string str = "Hello world!";
	T.SetLine(str);
	EXPECT_EQ(T.GetLine(), str);
}

TEST(TText, can_add_go_down_a_line)
{
	TTextLink::InitMemSystem(2);
	TText T;
	const string str = "Hello world!";
	T.GoFirstLink();
	T.InsDownLine(str);
	T.GoPrevLink();
	T.GoDownLink();
	EXPECT_EQ(T.GetLine(), str);
}

TEST(TText, can_see_an_error_in_the_absence_of_down)
{
	TTextLink::InitMemSystem(2);
	TText T;
	T.GoFirstLink();
	EXPECT_EQ(T.GoDownLink(), TextNoDown);
}

TEST(TText, can_see_an_error_in_the_absence_of_prev)
{
	TTextLink::InitMemSystem(2);
	TText T;
	T.GoFirstLink();
	EXPECT_EQ(T.GoPrevLink(), TextNoPrev);
}

TEST(TText, can_see_an_error_in_the_absence_of_next)
{
	TTextLink::InitMemSystem(2);
	TText T;
	T.GoFirstLink();
	EXPECT_EQ(T.GoNextLink(), TextNoNext);
}

TEST(TText, can_bring_a_positive_error_code)
{
	const string str = "Hello world!";
	TTextLink::InitMemSystem(2);
	TText T;
	T.GoFirstLink();
	T.InsDownLine(str);
	T.GoFirstLink();
	EXPECT_EQ(T.GoDownLink(), TextOK);
}

TEST(TText, can_del_down_line)
{
	const string str1 = "Hello world!";
	const string str2 = "Hello!";
	const string str3 = "World welcomes you";
	TTextLink::InitMemSystem(4);
	TText T;
	T.GoFirstLink();
	T.InsDownLine(str1);
	T.GoDownLink();
	T.InsDownLine(str2);
	T.InsDownLine(str3);
	T.GoDownLink();
	T.DelDownLine();
	T.GoFirstLink();
	T.GoDownLink();
	EXPECT_EQ(T.GetLine(), str1);
	T.GoDownLink();
	EXPECT_EQ(T.GetLine(), str3);
}

TEST(TText, can_del_down_section)
{
	const string sec1 = "section 1";
	const string sec2 = "section 1.1";
	const string sec3 = "section 1.2";
	const string str1 = "string 1";
	const string str2 = "string 2";
	TTextLink::InitMemSystem(6);
	TText T;
	T.GoFirstLink();
	T.InsDownLine(sec1);
	T.GoDownLink();
	T.InsDownLine(sec3);
	T.InsDownLine(sec2);
	T.GoDownLink();
	T.InsDownLine(str2);
	T.InsDownLine(str1);
	T.DelDownSection();
	T.GoFirstLink();
	T.GoDownLink();
	T.GoDownLink();
	EXPECT_EQ(T.GoDownLink(), TextNoDown);
}

TEST(TText, can_del_next_line)
{
	const string sec1 = "section 1";
	const string str1 = "string 1";
	const string str2 = "string 3";
	const string str3 = "string 2";
	TTextLink::InitMemSystem(5);
	TText T;
	T.GoFirstLink();
	T.InsDownLine(sec1);
	T.GoDownLink();
	T.InsDownSection(str1);
	T.GoDownLink();
	T.InsNextLine(str2);
	T.InsNextLine(str3);
	T.DelNextLine();
	T.GoFirstLink();
	T.GoDownLink();
	T.GoDownLink();
	EXPECT_EQ(T.GetLine(), str1);
	T.GoNextLink();
	EXPECT_EQ(T.GetLine(), str2);
}

TEST(TText, can_del_next_section)
{
	const string sec1 = "section 1";
	const string sec2 = "section 2";
	const string str1 = "string 1";
	const string str2 = "string 2";
	const string str3 = "string 3";
	TTextLink::InitMemSystem(7);
	TText T;
	T.GoFirstLink();
	T.InsDownLine(sec1);
	T.GoDownLink();
	T.InsNextLine(str2);
	T.InsNextLine(str1);
	T.InsNextSection(sec2);
	T.InsNextLine(str3);
	T.Write("dewv.txt");
	T.GoNextLink();
	T.DelNextSection();
	T.GoFirstLink();
	T.GoDownLink();
	T.GoNextLink();
	EXPECT_EQ(T.GetLine(), str3);
}
```

**Результат**

![](tests.jpg)

## Выводы

В ходе выполнения данной работы был получен некоторый опыт командной разработки и опыт реализации текстов на основе иерархического связанного списока. Также были реализованы некоторые операции с текстом. Система протестирована и готова.