#pragma once

#include <stack>
#include <string>
#include <fstream>
#include "TTextLink.h"
#include "TDataCom.h"

class TText;
typedef TText *PTText;

class TText : public TDataCom {
protected:
	PTTextLink pFirst;      // ��������� ����� ������
	PTTextLink pCurrent;      // ��������� ������� ������
	stack< PTTextLink > Path; // ���� ���������� �������� �� ������
	stack< PTTextLink > St;   // ���� ��� ���������
	PTTextLink GetFirstAtom(PTTextLink pl); // ����� ������� �����
	void PrintText(PTTextLink ptl);         // ������ ������ �� ����� ptl
	void PrintTextFile(PTTextLink ptl, ofstream& TxtFile); // ������ � ����
	PTTextLink ReadText(ifstream &TxtFile); //������ ������ �� �����
public:
	TText(PTTextLink pl = NULL);
	~TText() { pFirst = NULL; }
	PTText getCopy();
	// ���������
	int GoFirstLink(); // ������� � ������ ������
	int GoDownLink();  // ������� � ��������� ������ �� Down
	int GoNextLink();  // ������� � ��������� ������ �� Next
	int GoPrevLink();  // ������� � ���������� ������� � ������
	// ������
	string GetLine();   // ������ ������� ������
	void SetLine(string s); // ������ ������� ������ 
	// �����������
	void InsDownLine(string s);    // ������� ������ � ����������
	void InsDownSection(string s); // ������� ������� � ����������
	void InsNextLine(string s);    // ������� ������ � ��� �� ������
	void InsNextSection(string s); // ������� ������� � ��� �� ������
	void DelDownLine();        // �������� ������ � ���������
	void DelDownSection();     // �������� ������� � ���������
	void DelNextLine();        // �������� ������ � ��� �� ������
	void DelNextSection();     // �������� ������� � ��� �� ������
	// ��������
	int Reset();              // ���������� �� ������ �������
	bool IsTextEnded() const;  // ����� ��������?
	int GoNext();             // ������� � ��������� ������
	//������ � �������
	void Read(char * pFileName);  // ���� ������ �� �����
	void Write(char * pFileName); // ����� ������ � ����
	//������
	void Print();             // ������ ������
};